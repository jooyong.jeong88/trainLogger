'use strict';

angular.module('myApp.services', ['ngRoute'])

.factory('Programs', function(){
	return [
		{
			"date": "2012-02-03",
			"name": "JAKE's killer workout",
			"exercises": [
				{
				"name": "BenchPress",
				"weight": 234,
				"sets": 10,
				"reps": 20
				},

				{
				"name": "Pull-up",
				"weight": 12,
				"sets": 15,
				"reps": 23
				}
			]
		},

		{
			"date": "2012-03-04",
			"name": "Mike's bitch workout",
			"exercises": [
				{
				"name": "BenchPress",
				"weight": 234,
				"sets": 10,
				"reps": 20
				},

				{
				"name": "Pull-up",
				"weight": 12,
				"sets": 15,
				"reps": 23
				}
			]
		}
	
	];
});
