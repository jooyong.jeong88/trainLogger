'use strict';

angular.module('myApp.view1', ['ngRoute', 'ngMaterial'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', function($scope, $mdDialog, Programs) {


	var alert;

	$scope.exercises = [{
					"name": "",
					"weight": "",
					"sets": "",
					"reps": ""
				}];

	var program = Programs;
	//Function to add Exercise
	$scope.addExercises = function(a, b, c, d) {		
		$scope.exercises.push(
				{
					"name": a,
					"weight": b,
					"sets": c,
					"reps": d
				}	
			)		
	};
	//Function to Delete Exercise
	$scope.deleteExercise = function(item) {
		var index = $scope.exercises.indexOf(item);		
		$scope.exercises.splice(index, 1);
	};
	//Function to Save Program
	$scope.saveProgram =  function(){
		program.push(
			{
			"date": "2012-02-03",
			"name": "JAKE's assKiller workout",
			"exercises": $scope.exercises
			}
		)
	}
	//Function to reset program
	$scope.resetProgram = function(){
		$scope.exercises = [{
					"name": "",
					"weight": "",
					"sets": "",
					"reps": ""
				}];
	}

	 $scope.showAlert = function(ev) {
    // Appending dialog to document.body to cover sidenav in docs app
    // Modal dialogs should fully cover application
    // to prevent interaction outside of dialog
    	$mdDialog.show(
      	$mdDialog.alert()
        .parent(angular.element(document.querySelector('#popupContainer')))
        .clickOutsideToClose(true)
        .title('This is an alert title')
        .textContent('You can specify some description text in here.')
        .ariaLabel('Alert Dialog Demo')
        .ok('Got it!')
        .targetEvent(ev)
    );
  };
});