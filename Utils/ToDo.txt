CURRENT WORKS
1. When saving the program, validates if there is any empty part in input box.
2. Design Improvement.
3. When saving the workout, make pop up for asking name and validates if the input name is empty.
4. Reset button in creating exercise - Done
5. Implement feature so that previous workout can be used as template






FUTURE WORKS
0. Implement our own backend server, so task 1 and 2 will become possible (I have my eye on aws).
1. Create Database so user can choose exercise from given list. This will make task #2 a lot easier.
2. Automatically load exercise's last history (i.e., Sets, Reps, Weight).
