TrainLogger V0.1

- This is personal project to apply and practice my software engineering skills.
- However, it is also an application that I always wanted create because I am a gym rat, and I always wanted to have simple training log.
- To start application
  - For AngularJS version: You can only run this locally for now. 
  - For commandline version : simply compile "Main.java", and execute it. (i.e., 'javac Main.java' then 'java Main').
- Branching Strategy
  - master: contains source code for angularJS version of application.
  - commandline: contains source code for commandline version of application.
- For any inquiries, contact me at jooyong.jeong88@gmail.com

Love,
Jake
